import {BanktransactieFilters} from "../../../models/models";

export const defaultBanktransactieFilters: BanktransactieFilters = {
	onlyUnbooked: true,
	isCredit: "all",
};